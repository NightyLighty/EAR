# Experimental Cipher 1

import hashlib
from numpy import random

def randint(rng, low, high):
    return rng.integers(low=low, high=high, size=1)[0]

def gen_n(n1, n2):
    encoding = 'utf-8'
    rng1 = random.default_rng(n1)
    rng2 = random.default_rng(n2)
    s1 = bytes(str(n1), encoding)
    s2 = bytes(str(n2), encoding)
    # modified from https://stackoverflow.com/a/16008760
    hash1 = int(hashlib.sha512(s1).hexdigest(), 16) % 10 ** 8
    hash2 = int(hashlib.sha512(s2).hexdigest(), 16) % 10 ** 8
    return abs(randint(rng1, 0, hash1) - randint(rng2, 0, hash2))

def init(pw):
    i = 0
    l = list(range(256))
    length = len(pw)
    for j in range(256):
        i = (i + l[j] + pw[j % length]) % 256
        l[i], l[j] = l[j], l[i]
    return l

def encrypt(msg, pw):
    s = init(pw)
    rng1 = random.default_rng(len(s))
    rng2 = random.default_rng(gen_n(randint(rng1, 0, len(s)), len(s)))
    i = 0
    j = 0
    l1 = []
    l2 = []
    for k in range(len(msg)):
        rng = rng1 if k % 2 else rng2
        i = (i + randint(rng, 1, i + 2)) % 256
        j = (j + s[i]) % 256
        s[i], s[j] = s[j], s[i]
        l1.append(s[(s[i] + s[j]) % 256])
        l2.append(msg[k] ^ l1[k])
    return bytes(l2)

def decrypt(msg, pw):
    s = init(pw)
    rng1 = random.default_rng(len(s))
    rng2 = random.default_rng(gen_n(randint(rng1, 0, len(s)), len(s)))
    i = 0
    j = 0
    l1 = []
    l2 = []
    for k in range(len(msg)):
        rng = rng1 if k % 2 else rng2
        i = (i + randint(rng, 1, i + 2)) % 256
        j = (j + s[i]) % 256
        s[i], s[j] = s[j], s[i]
        l1.append(s[(s[i] + s[j]) % 256])
        l2.append(msg[k] ^ l1[k])
    return bytes(l2)

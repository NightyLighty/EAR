# Wow Better Something

import string

def WBS(inp):
    counter = tapeCounter = 0
    tape = {}
    res = []
    inplen = len(inp)
    while counter < inplen:
        char = inp[counter]
        if char == 0:
            counter += 1
            if counter == inplen:
                raise Exception("Unexpected end of input")
            new_char = inp[counter]
            chars = []
            while new_char > 3 and counter < inplen:
                chars.append(new_char)
                counter += 1
                if counter < inplen:
                    new_char = inp[counter]
            counter -= 1
            tapeCounter = bytes(chars)
            tape[tapeCounter] = tape.get(tapeCounter) or 0
        elif char == 1:
            tape[tapeCounter] += 1
        elif char == 2:
            tape[tapeCounter] -= 1
        elif char == 3:
            res.append(tape[tapeCounter])
        counter += 1
    return bytes(v % 256 for v in res)

if __name__ == "__main__":
    print(WBS(b"\x00\x04\x01\x03\x00\x05\x01\x01\x03\x00\x04\x02\x03"))

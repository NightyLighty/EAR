# Cryptography Giga 1
# Encryption Algorithm

# Inspired by RC4

def KSA(key: bytes):
    s = list(range(512))
    j = 0
    length = len(key)
    for i in range(512):
        a = s[i]
        j = (j + a) % 512
        b = s[j]
        s[j] = s[i]
        s[i] = b
        c = s[((i << 5) ^ (j >> 3)) % 512] + key[((j << 5) * (i >> 3)) % length] | s[(a & b * s[i * j % 512]) % 512]
        s[i] = (s[(a + b) % 512] + key[(c * b * a) % length] * a) ^ s[(j + b) % 512]
 
    return s

def PRGA(text: bytes, s: list) -> bytes:
    res = []
    i = j = k = l = m = n = 0
    for t in text:
        i = (i + 1) % 512
        m = j ^ l
        n = i * k
        j = (j + s[i]) % 512
        l = (l + s[i] ^ s[j]) % 512
        s[i], s[j] = s[j], s[i]
        xor_key = s[(s[i] + s[j]) % 512]
        s[l], s[j] = s[j], s[l]
        xor_key = xor_key ^ s[(s[j] + s[l]) % 512]
        k = (k + s[(i << 5 + j >> 3) % 512]) % 512
        res.append((xor_key ^ t ^ k + n ^ m) % 256)
 
    return bytes(res)

def CG1(txt: bytes, key: bytes) -> bytes:
    return PRGA(txt, KSA(key))

if __name__ == "__main__":
    import timeit
    text = b"This is a test"
    key = b"Any key Any key Right"
    encrypted = CG1(text, key)
    print(timeit.timeit(lambda: CG1(text, key), number=10000))
    print(text)
    print(key)
    print(encrypted)
    decrypted = CG1(encrypted, key)
    print(timeit.timeit(lambda: CG1(encrypted, key), number=10000))
    print(decrypted)
    print(len(encrypted) / len(text))

import math

# PENGUIN NUMBER GENERATOR
# A simple experimental PRNG

class PNG:                                           
    def __init__(self, seed):                                       
        self.seed = seed
        self.div = 2 ** 64
        for i in range(10):
            self.random()
    def random(self):
        self.update()
        return (self.seed % self.div) / self.div
    def random01(self):
        self.update()
        return (self.seed % (self.div + 1)) / self.div
    def reverse(self):
        string = str(self.seed)
        return int(string[::-1].replace("0", str(int(string[0]) + 1)[0]))
    def update(self):
        self.seed += self.seed ^ self.reverse()
        self.seed *= int(1 + math.sqrt(self.seed))
        self.seed %= self.div + 1

if __name__ == "__main__":
    from RST import RST
    print(RST(PNG))

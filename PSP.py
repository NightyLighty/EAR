# Penguin Search Penguin
# Only works for sorted array

_min = min
_len = len

# https://codezup.com/binary-search-algorithm-sorted-list-loop-python/
def binarySearch(target, List):
    left = 0
    right = _len(List) - 1
    
    while left <= right:
        mid = (left + right) // 2
        if target == List[mid]:
            return mid
        elif target < List[mid]:
            right =  mid - 1
        else:
            left = mid + 1

def PSP(_list, target):
    length = _len(_list)
    low, high = 0, length - 1
    s = low + high
    small = s // 3
    large = _min((s * 2) // 3, length - 1)
    first = _list[small] > target
    second = _list[large] < target

    while first or second:
        if first:
            high = small
        else:
            low = large
        s = low + high
        mid = s // 2
        val = _list[mid]
        if target == val:
            return mid
        small = s // 3
        large = _min((s * 2) // 3, length - 1)
        first = _list[small] > target
        second = _list[large] < target
    
    return small + binarySearch(target, _list[small + 1:large]) + 1

# RandomnesS Test

import math, random
from itertools import combinations

def test(inp):
    s = sum(inp)
    length = len(inp)
    mean = s / length
    res = 1 - abs(0.5 - mean) * 2
    dist = list(bytes(10))
    for v in inp:
        dist[math.floor(v / 0.1)] += 1
    mean2 = length / 10
    for v in dist:
        res += 1 - abs(mean2 - v) / mean2
    combs = list(combinations(random.choices(inp, k=100), 2))
    avg = list(bytes(10))
    for comb in combs:
        absolute = abs(comb[0] - comb[1])
        avg[math.floor(absolute / 0.1)] += 1
    mean3 = len(combs) / 10
    for average in avg:
        res += 1 - abs(mean3 - average) / mean3
    return res / 21

def RST(fn, iters=100, number=10000):
    res = 0
    for i in range(iters):
        seed = random.randint(1, 100)
        rng = fn(seed)
        test_l = []
        for j in range(number):
            test_l.append(rng.random())
        res += test(test_l)
    return res / 100

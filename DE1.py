# Data Encryptor 1

import hashlib, random, EC1, Guinoi, zlib

default_salt = b"Data Encryptor 1 is the worst I have ever seen"

def _xor(in1, in2):
    while len(in2) < len(in1):
        in2 = in2 * 2
    in2 = in2[:len(in1)]

    return bytes([a ^ b for a, b in zip(in1, in2)])

def _hash(txt, salt):
    encoded = hashlib.sha512(txt + salt).digest()[31:33]
    encoded += hashlib.md5(txt + salt).digest()[7:9]
    encoded += hashlib.blake2b(txt + salt).digest()[31:33]
    return encoded

def _feistel_cipher(message, keys):
    second_half_index = len(message) // 2
    n_left = message[:second_half_index]
    n_right = message[second_half_index:]
    for key in keys:
        n_right_tmp = n_right
        n_right = _xor(n_left, _hash(n_right_tmp, key))
        n_left = n_right_tmp

    return n_right + n_left

def encrypt(msg, pw):
    message = zlib.compress(msg, level=9)
    add = b""
    if len(message) % 2:
        add = bytes([message[-1]])
        message = message[:-1]
    length = len(pw)
    keys = []
    for i in range(0, len(pw), 8):
        keys.append(pw[i:i + 8])
    crypt_msg = _feistel_cipher(message, keys)
    for i in range(32):
        crypt_msg += _hash(crypt_msg, default_salt)
        for key in keys:
            key = _hash(key, default_salt)
            crypt_msg = EC1.encrypt(crypt_msg, key)
        crypt_msg = _feistel_cipher(crypt_msg, keys)
    nex = bytes([((add or b' ')[0] + 1) % 256])
    return add + nex + zlib.compress(crypt_msg, level=9) + nex

def decrypt(encoded_msg, pw):
    length = len(pw)
    nex = encoded_msg[-1]
    ind = encoded_msg.index(nex)
    add = encoded_msg[:ind]
    encoded_msg = encoded_msg[ind + 1:-1]
    encoded_message = zlib.decompress(encoded_msg)
    keys = []
    for i in range(0, len(pw), 8):
        keys.append(pw[i:i + 8])
    decoded_message = _feistel_cipher(encoded_message, keys[::-1])
    for i in range(32):
        decoded_message = decoded_message[:-6]
        for key in keys[::-1]:
            key = _hash(key, default_salt)
            decoded_message = EC1.decrypt(decoded_message, key)
        decoded_message = _feistel_cipher(decoded_message, keys[::-1])
    return zlib.decompress(decoded_message + add)

if __name__ == "__main__":
    message = bytes(random.randint(0, 255) for _ in range(32))
    key = bytes(random.randint(0, 255) for _ in range(32))

    encrypted = encrypt(message, key)
    print(message)
    print(encrypted)

    decoded = decrypt(encrypted, key)
    print(decoded)
    
    print(len(encrypted) / len(message))

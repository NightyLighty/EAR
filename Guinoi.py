import re

def find_all(string, substr):
    return re.finditer(re.escape(substr), string)

alpha = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

def _encode(n):
    res = ""
    while n:
        n, r = n // 62, n % 62
        res = res + alpha[r]
    return res or alpha[0]

def _decode(s):
    length = len(s)
    n = 0
    for i, c in enumerate(s):
        exp = length - i - 1
        n += alpha.index(c) * (62 ** exp)
    return n

def encode(msg):
    res = []
    found = []
    for i, v in enumerate(msg):
        if v not in found:
            found.append(v)
            finds = find_all(msg, v)
            l = {}
            for val in finds:
                encoded = _encode(val.start())
                length = len(encoded)
                l[length] = l.get(length) or []
                l[length].append(encoded)
            for length, vals in l.items():
                res.append(f"{_encode(length)}|{v}{''.join(vals)}")
    return " ".join(res)

def decode(msg):
    message = msg.split(" ")
    res = []
    for i, v in enumerate(message):
        length, data = v.split("|")
        index = data[1:]
        length = _decode(length)
        char = data[0]
        for j in range(0, len(index), length):
            leng = len(res)
            ind = _decode(index[j:j + length])
            if leng < ind + 1:
                res += ["" for inde in range(ind + 1 - leng)]
                res[-1] = char
            else:
                res[ind] = char
    return "".join(res)

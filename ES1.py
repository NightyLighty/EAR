# Experimental Sort 1

import random

# https://stackoverflow.com/a/3755251
def is_sorted(l):
    return all(l[i] <= l[i+1] for i in range(len(l) - 1))

def gen_new(l):
    return [int(random.choice(str(v))) for v in l]

def sort_part(l):
    new_list = gen_new(l)
    new_list.sort()
    
    # modified from https://stackoverflow.com/a/3633311
    return [x for x in new_list if x in l] + [x for x in l if x not in new_list]

def sort(l):
    new_list = sort_part(l)
    
    while not is_sorted(new_list):
        new_list = sort_part(new_list)
    return new_list

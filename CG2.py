# Cryptography Giga 2
# Hash Algorithm
# The worst I have ever seen

import zlib

def _CG2(text: bytes, salt: bytes) -> bytes:
    txt = []
    for t, s in zip(text, salt):
        txt.append(t ^ s)
    txt = zlib.compress(bytes(txt), level=9)
    len1 = len(txt)
    txt = list(txt)
    slt = list(salt[::-1])
    for t, s in zip(txt, slt):
        txt[t * s % len1] = t & s
    txt = zlib.compress(bytes(txt), level=9)
    txt = list(txt)
    j = 0
    len2 = len(slt)
    for i in range(len2):
        j = (j | (i * slt[i])) % len2
        slt[i], slt[j] = slt[j], slt[i]
    for t, s in zip(txt, slt):
        txt.append(t * s % 256)
    txt = zlib.compress(bytes(txt), level=9)
    return txt

def CG2(text: bytes, salt: bytes, n: int=10000) -> bytes:
    if len(salt) != 64:
        raise Exception("Invalid salt")
    res = text
    for i in range(n):
        res = _CG2(res, salt)
    return salt + res[:64]

if __name__ == "__main__":
    from random import randint
    hashed = CG2(b"text", bytes(randint(0, 255) for i in range(64)))
    print(hashed)
    print(len(hashed))
